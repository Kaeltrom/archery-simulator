﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveTracker : MonoBehaviour {

	public FlowControl flowControl;
	public StoreTimers storeTimersScript;
	public Image objectiveDoneImage;
	public Sprite tick, cross;
	public bool objectiveComplete;
    public AudioSource audioSource;

    private bool soundPlayed;

    // Use this for initialization
    void Start ()
    {
		objectiveDoneImage.sprite = cross;
		soundPlayed = false;
		objectiveComplete = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (objectiveComplete == true && soundPlayed == false) 
		{
			objectiveDoneImage.sprite = tick;
			audioSource.Play();
			storeTimersScript.WriteTimer("Paco", flowControl.objectiveActive.ToString());
			flowControl.SetActiveObjective(flowControl.objectiveActive + 1);
			soundPlayed = true;
		}
	}
}
