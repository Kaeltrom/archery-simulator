﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using UnityEngine.UI;

public class StoreTimers : MonoBehaviour {

	public Text timerText, numberOfArrows;

	void Start()
	{
		WriteOnStart ();
	}

	void WriteOnStart()
	{
		using (StreamWriter saveTimer = new StreamWriter ("Assets/Resources/Texts/Timers.csv", true)) 
		{
			//Check if the file is empty. If it is, create the headers
			if (new FileInfo ("Assets/Resources/Texts/Timers.csv").Length == 0)
				saveTimer.Write ("Subject,Objective,Time,Arrows");
			else
				saveTimer.Write ("\r\n");
		}
	}

	public void WriteTimer(string subjectName, string objective)
	{
		//Add text to the file
		using (StreamWriter saveTimer = new StreamWriter ("Assets/Resources/Texts/Timers.csv", true)) 
		{
			//Check if the file is empty. If not, add a new line
			if (new FileInfo ("Assets/Resources/Texts/Timers.csv").Length != 0)
				saveTimer.Write ("\r\n");
			
			//saveTimer.Write ("Objective " + objective + " - " + timerText.text);
			saveTimer.Write (subjectName + "," + "Objective " + objective + "," + timerText.text + "," + numberOfArrows.text);
		}
	}
}
