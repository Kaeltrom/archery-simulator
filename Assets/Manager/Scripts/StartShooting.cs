﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StartShooting : MonoBehaviour {

	public GameObject backWall;
	public FlowControl flowControlScript;

	void OnTriggerEnter(Collider whatEnter)
	{
		if (whatEnter.CompareTag("Player")) 
		{
			backWall.SetActive (true);
			flowControlScript.SetActiveObjective(1);
		}
	}
}
