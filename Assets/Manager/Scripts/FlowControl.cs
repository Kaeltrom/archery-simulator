﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FlowControl : MonoBehaviour {

	public GameObject objectiveBlockOne, objectiveBlockTwo, objectiveBlockThree;

	[HideInInspector]
	public int objectiveActive = 0;

    public void SetActiveObjective(int objectiveToActivate)
    {
        //Disable the collider blocking the objective set to be activated
        switch (objectiveToActivate)
        {
            case 1:
                objectiveBlockOne.SetActive(false);
                break;
            case 2:
                objectiveBlockTwo.SetActive(false);
                break;
            case 3:
                objectiveBlockThree.SetActive(false);
                break;
            default:
                break;
        }

        objectiveActive = objectiveToActivate;
    }
}
