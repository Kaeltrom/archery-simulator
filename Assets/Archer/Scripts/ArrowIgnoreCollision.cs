﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowIgnoreCollision : MonoBehaviour {

	public List<GameObject> chainReactColliders = new List<GameObject>();

	// Use this for initialization
	void Start ()
    {
		foreach (GameObject chainLimit in chainReactColliders) 
		{
			Physics.IgnoreCollision(chainLimit.GetComponent<Collider>(), GetComponent<Collider>());
		}
	}
}
