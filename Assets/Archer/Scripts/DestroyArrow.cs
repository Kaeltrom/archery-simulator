﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyArrow : MonoBehaviour {

	public GameObject Bow;

	// Use this for initialization
	void Start ()
    {
        //Set the arrow to ignore the collision with the bow in order to not being destroyed too early
		Physics.IgnoreCollision(Bow.GetComponent<Collider>(), GetComponent<Collider>());
	}

	void OnCollisionEnter(Collision col)
	{
		if (!col.transform.CompareTag("Bow") && !col.transform.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
	}
}
