﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowFly : MonoBehaviour {

	public bool isFlying = false;
	public float arrowPullTime;
	public float arrowShootingSpeed;

    private Rigidbody arrowRigidbody;

	private bool velocityNotSet = true;

    private void Start()
    {
        arrowRigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
	{
		if (isFlying && velocityNotSet) 
		{
			float arrowSpeed = arrowShootingSpeed;

            // Add velocity to the arrow depending on the time spent pulling it
            arrowRigidbody.isKinematic = false;
            arrowRigidbody.AddForce(transform.up * arrowSpeed * arrowPullTime, ForceMode.VelocityChange);
            arrowRigidbody.useGravity = true;
			velocityNotSet = false;
		}
	}
}
