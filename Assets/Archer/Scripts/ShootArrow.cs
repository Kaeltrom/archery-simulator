﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootArrow : MonoBehaviour {

	public AudioClip bowTension, bowShoot;
	public GameObject arrowModel;
	public GameObject bowModel;
	public int arrowShootingSpeed = 20;
	public Transform arrowSpawn;
	public Transform arrowBehind;
	public AudioSource bowAudioSource;
    public UIController uiController;

    private GameObject arrowToShoot;
	private float arrowPullTime = 0;
	private bool arrowCreated = false;
	
	// Update is called once per frame
	void Update()
	{
		//When player clicks, arrow is created
		if (!arrowCreated && (Input.GetMouseButtonDown (0) || Input.GetButtonDown("Submit"))) // || Input.GetAxis("Oculus_GearVR_RHandTrigger")>0.5f)) 
		{
			CreateArrow();
		}

		//When button is released, shoot the arrow
		if (arrowCreated && (Input.GetMouseButtonUp(0) || Input.GetButtonUp("Submit"))) //  || Input.GetAxis("Oculus_GearVR_RHandTrigger")<0.5f))

        {
			FireArrow();
		}
	}

	void FixedUpdate () 
	{
		//While mouse button is pushed, pull the arrow
		if (arrowCreated && (Input.GetMouseButton(0) || Input.GetButton("Submit"))) // || Input.GetAxis("Oculus_GearVR_RHandTrigger")>0.5f)) 

        {
			PullArrow ();
		}
	}

	void CreateArrow()
	{
		// Create the Arrow from the Arrow Model
		arrowToShoot = Instantiate(arrowModel, arrowSpawn.position, arrowSpawn.rotation);
		arrowToShoot.transform.SetParent (bowModel.transform);
        bowAudioSource.PlayOneShot(bowTension);
		arrowCreated = true;
	}

	void PullArrow()
	{
        if (arrowPullTime < 1)
        {
            arrowPullTime += Time.deltaTime;
        }
        else if (arrowPullTime > 1)
        {
            arrowPullTime = 1;
        }

        //Lerp the arrow position to simulate the arrow pull
        if (arrowToShoot != null)
        {
            arrowToShoot.transform.position = Vector3.Lerp(arrowSpawn.position, arrowBehind.position, arrowPullTime);

        }
    }

	void FireArrow()
	{
		if (arrowToShoot != null) 
		{
			//Play the shoot sound
            bowAudioSource.PlayOneShot(bowShoot);

			//Add one arrow to the count
			uiController.AddArrowToCount();

			//Detach from parent to prevent the arrow from changing directions if player moves
			DetachArrowFromParent(arrowToShoot);

			//Set the arrow to update
			ArrowFly arrowFlyComponent = arrowToShoot.GetComponent<ArrowFly>();
			arrowFlyComponent.isFlying = true;
			arrowFlyComponent.arrowPullTime = arrowPullTime;
			arrowFlyComponent.arrowShootingSpeed = arrowShootingSpeed;

			//Reset arrow pull time so the next arrow spawns on the initial position
			arrowPullTime = 0;

			// Destroy the arrow after 2 seconds
			Destroy(arrowToShoot, 2.0f);

			//Set the variable to null, so it isn't affected by clics anymore
			arrowToShoot = null;
			arrowCreated = false;
		}  
	}

    public void DetachArrowFromParent(GameObject arrow)
    {
        arrow.transform.parent = null;
    }
}
