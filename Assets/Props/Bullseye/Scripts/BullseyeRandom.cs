﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BullseyeRandom : MonoBehaviour {

	public float minPosY, maxPosY, minPosZ, maxPosZ;
	public float moveSpeedUpDown, moveSpeedLeftRight;

	private int goUpDown = 1;
	private int goLeftRight = 0;
	private int difficultyLevel = 1;
	private Rigidbody bullseyeRigidbody;
    private ObjectiveTracker objectiveTracker;

	// Use this for initialization
	void Start ()
    {
        bullseyeRigidbody = GetComponent<Rigidbody> ();
        objectiveTracker = GetComponentInParent<ObjectiveTracker>();
    }

	void FixedUpdate () {
		if (transform.localPosition.y >= maxPosY) 
		{
			transform.localPosition = new Vector3(transform.localPosition.x, maxPosY, transform.localPosition.z);
			goUpDown = -1;
		}
		else if (transform.localPosition.y <= minPosY) 
		{
			transform.localPosition = new Vector3(transform.localPosition.x, minPosY, transform.localPosition.z);
			goUpDown = 1;
		}

		if (difficultyLevel >= 2) 
		{
			if (transform.localPosition.z >= maxPosZ) 
			{
				transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, maxPosZ);
				goLeftRight = -1;
			}
			else if (transform.localPosition.z <= minPosZ) 
			{
				transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, minPosZ);
				goLeftRight = 1;
			}
		}

		if(objectiveTracker.flowControl.objectiveActive == 3)
        {
            BullseyeMove();
        }
	}

	void BullseyeMove()
	{
        bullseyeRigidbody.velocity = new Vector3(bullseyeRigidbody.velocity.x, moveSpeedUpDown * goUpDown, moveSpeedLeftRight * goLeftRight);
	}

	void OnCollisionEnter(Collision col)
	{
		GetComponent<AudioSource>().Play ();

		difficultyLevel += 1;

		if (difficultyLevel == 2)
			goLeftRight = 1;

		if (difficultyLevel == 3) 
		{
			moveSpeedUpDown *= 2;
			moveSpeedLeftRight *= 2;
		}

        if(difficultyLevel > 3)
        {
            objectiveTracker.objectiveComplete = true;
        }
	}
}
