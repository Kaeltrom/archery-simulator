﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightQuad : MonoBehaviour {

	public GameObject flySphere;
	public Color32 black;
    public ObjectiveTracker objective2Tracker;

    private bool partyStarted = false;
    private Renderer quadRenderer;

    void Start()
    {
        quadRenderer = GetComponent<Renderer>();
    }

	void OnCollisionEnter(Collision col)
	{
		if(col.transform.CompareTag("Light Trigger")) 
		{
			quadRenderer.material.color = black;

			//Set objective as complete
			objective2Tracker.objectiveComplete = true;

			if (!partyStarted)
            {
                StartCoroutine(ParticleParty());
            }
		}
	}

	IEnumerator ParticleParty()
	{
		partyStarted = true;
		yield return new WaitForSecondsRealtime(2);

		//Start particle party
		flySphere.GetComponent<FlySphere>().partyStarted = true;
		transform.GetChild(0).gameObject.SetActive(true);
	}
}
