﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlySphere : MonoBehaviour {

	public bool partyStarted = false;

    private Rigidbody sphereRigidbody;

    private void Start()
    {
        sphereRigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
	{
		if(partyStarted)
        {
            FlyYouFool();
        }
	}

	void FlyYouFool()
	{
		sphereRigidbody.AddForce(new Vector3(0, 10, 0));
	}
}
