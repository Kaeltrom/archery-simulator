﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DominoPieceCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision col)
	{
		if (col.transform.gameObject.tag == "Arrow" || col.transform.gameObject.tag == "Domino") 
		{
			GetComponent<Renderer> ().material.color = new Color32((byte)Random.Range (0, 256), (byte)Random.Range (0, 256), (byte)Random.Range (0, 256), (byte)Random.Range (0, 256));
		}
	}
}
