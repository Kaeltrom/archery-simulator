﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelDestroy : MonoBehaviour {

	public GameObject explosionEffect;
    public ObjectiveTracker objective1Tracker;
    public AudioSource objectiveAudioSource;

    private GameObject explosionSystem;

	void OnCollisionEnter(Collision col)
	{
		if (col.transform.gameObject.tag == "Arrow" || col.transform.gameObject.tag == "Domino") 
		{
            Explode();
		}
	}

	void Explode()
	{
		//Set objective as complete
		objective1Tracker.objectiveComplete = true;

		//Show explosion effect
		explosionSystem = Instantiate(explosionEffect, transform.position, transform.rotation, transform.parent);

		//Play explosion sound
		objectiveAudioSource.Play();

        Destroy(explosionSystem, 3);
        Destroy(gameObject);
    }
}
