﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetCollision : MonoBehaviour {

	public Material targetColor;
	public Color32 originalColor, colorToChange;

	void OnCollisionEnter(Collision col)
	{
		if (col.transform.gameObject.tag == "Arrow") 
		{
			targetColor.SetColor("_EmissionColor", colorToChange);
			transform.Rotate(-10.0f, 0, 0);
		}
	}

	void OnApplicationQuit()
	{
		targetColor.SetColor("_EmissionColor", originalColor);
	}
}
