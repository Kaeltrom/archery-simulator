﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMe : MonoBehaviour {

	void OnCollisionEnter(Collision col)
	{
		if (col.transform.gameObject.tag == "Arrow") 
		{
			Destroy (gameObject);
		}
	}
}
