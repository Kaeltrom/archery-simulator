﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	public static bool gameIsPaused;
	public GameObject pauseMenuUI;
	public GameObject bow;

	// Use this for initialization
	void Start ()
    {
		Resume ();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.P))
		{
            if (gameIsPaused)
            {
                Resume();
            }

            else
            {
                Pause();
            }
        }
	}

	public void Resume()
	{
		bow.SetActive(true);
		pauseMenuUI.SetActive(false);
		Time.timeScale = 1;
		gameIsPaused = false;
		Cursor.visible = false;
	}

	void Pause()
	{
		bow.SetActive(false);
		pauseMenuUI.SetActive(true);
		Time.timeScale = 0;
		gameIsPaused = true;
		Cursor.visible = true;
	}

	public void MainMenu()
	{
		SceneManager.LoadScene("Main Menu");
	}

	public void Close()
	{
		Application.Quit();
	}
}
