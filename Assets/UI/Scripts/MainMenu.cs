﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	public GameObject loadingBar;
	public Slider slider;
	public Text progressText;

	// Use this for initialization
	void Start ()
    {
		loadingBar.SetActive(false);
	}

	public void GoToField()
	{
		StartCoroutine (LoadAsynchronously());
	}

	public void Exit()
	{
		Application.Quit ();
	}

	IEnumerator LoadAsynchronously()
	{
		AsyncOperation loading = SceneManager.LoadSceneAsync(1);

		loadingBar.SetActive (true);

		while (!loading.isDone) 
		{
			float progress = Mathf.Clamp01 (loading.progress / .9f);

			slider.value = progress;
			progressText.text = Mathf.Round(progress * 100) + "%";

			yield return null;
		}
	}
}
