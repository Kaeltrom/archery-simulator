﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	public Text numberOfArrows;
	int numberOfArrowsCount;

	// Use this for initialization
	void Start ()
    {
		numberOfArrows.text = "0";
	}

	public void AddArrowToCount()
	{
		numberOfArrowsCount += 1;
		numberOfArrows.text = numberOfArrowsCount.ToString();
	}
}
