﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeControl : MonoBehaviour {

	public Text timeText;

	private float timeStart;

	// Use this for initialization
	void Start () {
		timeText.text = "0:00";
		timeStart = Time.time;
	}
	
	// Update is called once per frame
	void Update ()
    {
		float t = Time.time - timeStart;

		string minutes = ((int)t / 60).ToString ();
		string seconds = (t % 60).ToString ("f0");
		float secondsAux = t % 60;

		if (secondsAux <= 9.5f)
			seconds = "0" + seconds;

		timeText.text = minutes + ":" + seconds;
	}
}
